<?php

return array(
	'host' => 'server_ip',
	'user' => 'username',
	'password' => 'password',
	'database' => 'database',
);
