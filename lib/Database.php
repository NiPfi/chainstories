<?php

/**
 * Created by PhpStorm.
 * User: Nicola
 * Date: 19.05.17
 * Time: 12:30
 *
 * base database connection class used for inheritance to more specific database classes
 */
abstract class Database {
	
	protected $tableName;

    /**
     * opens up a mysql connection
     * @return mysqli connection
     */
	protected function openConnection() {
		
		$configuration = require '../secrets/db.inc';

		$mysqli = mysqli_connect(
			$configuration['host'],
			$configuration['user'],
			$configuration['password'],
			$configuration['database']
		);
		
		if(mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		$mysqli->set_charset('utf8');
		$mysqli->query("USE `chainstories`;");
		return $mysqli;
		
	}

    /**
     * reads all attributes form the set table
     * @return array|bool|mysqli_result returns the result returned from the database
     * @throws Exception if the statement has an error this exception is thrown
     */
	public function readAll() {
		$mysqli = $this->openConnection();
		$rows = array();
		if ($stmt = $mysqli->prepare("SELECT * FROM ($this->tableName)")) {
			$stmt->execute();
			
			$result = $stmt->get_result();
			
			$this->checkForError($stmt);
			
			$stmt->close();
			
			$rows = $result;
			
		}
		$mysqli->close();
		return $rows;
	}

    /**
     * searches for a row in the database where the given attribute equals the given value and returns it
     * @param $attribute attribute to compare with the value
     * @param $value value that is searched for in the given attribute
     * @return mixed|null returns the result returned from the database
     * @throws Exception if the statement has an error this exception is thrown
     */
	protected function readBy($attribute, $value) {
		$mysqli = $this->openConnection();
		$row = null;
		if ($stmt = $mysqli->prepare("SELECT * FROM ($this->tableName) WHERE $attribute=?")) {
			$types = null;
			
			switch (true) {
				case is_integer($value):
					$types = 'i';
					break;
				default:
					$types = 's';
					break;
			}
			
			$stmt->bind_param($types, $value);
			$stmt->execute();
			
			$result = $stmt->get_result();
			
			$this->checkForError($stmt);
			
			$stmt->close();
			
			$row = $result->fetch_array();
			
		}
		
		$mysqli->close();
		return $row;
	}

    /**
     * dynamically call methods like readBy'Attribute' so you can magically use
     * those methods even though they technically don't exist
     * @author Sam aka soxyl
     * @param $method generic Method name that starts with readBy
     * @param $args arguments that will be passed to the readBy method
     * @return mixed|null returns the return value of the generic readBy'Attribute' method
     */
	public function __call($method, $args) {
		if(substr($method,0,6) === 'readBy') {
			$attribute = lcfirst(substr($method,6));
			if (in_array($attribute, $this->getCols()))
			return $this->readBy($attribute, $args[0]);
		}
	}

    /**
     * the accepted attributes should be identical to the ones existing in the corresponding database table
     * @return returns an array with all accepted attributes for the creation of the generic readBy Methods
     */
	protected abstract function getCols();
	
	/**
	 * Checks if a statement has produced an SQL error and throws it in an exception.
	 *
	 * @param $stmt mysqli_stmt to check
	 * @throws Exception
	 */
	public function checkForError($stmt) {
		if(!isset($stmt)) {
			$class = get_class();
			throw new Exception("Invalid statement in $class");
		}
		$error = $stmt->error;
		if ($error !== "") {
			throw new Exception($error);
		}
	}
	
}