<?php
/**
 *
 * View class
 *
 */
class View
{
    private $viewfile;
    public function __construct($viewfile)
    {
        $this->viewfile = "./../view/$viewfile.php";
    }
    public function display()
    {
        require $this->viewfile;
    }

}