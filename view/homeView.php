<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home</title>

    <link href="/css/normalize.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/scss/style.css" rel="stylesheet">

</head>
<body class="container-fluid">

<?php

if(isset($_SESSION['user'])) {
	
	require_once "loggedInHeader.php";
	
} else {
	
	require_once "defaultHeader.html";
}

require_once "message.inc";

?>

<main class="row">

    <?php
    require_once "aside.php";

    require_once "content.php";
    ?>

</main>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

</body>
</html>