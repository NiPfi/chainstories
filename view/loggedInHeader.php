<header class="row">

    <div id="logo" class="col-2">
        <a href="./../" class="btn" role="button"><h1>Chain Stories</h1></a>
    </div>

    <div id="headerContent" class="col-10">

        <div class="form-inline">
        <?php
            echo '<a href="/user/editUser"><b>Hello, ' . $_SESSION["user"]["username"].'</b></a>';
        ?>
            <a href="/user/logout" class="btn btn-default" role="button">LogOut</a>
        </div>
    </div>

</header>