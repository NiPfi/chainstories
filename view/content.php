<?php

require_once './../repositories/StoryTable.php';
require_once './../repositories/StoryPartTable.php';
require_once './../repositories/GenreTable.php';

?>

<div id="storyDiv" class="col-10">

    <?php

    $storyTable = new StoryTable();
    $storyPartTable = new StoryPartTable();
    $genreTable = new GenreTable();
    $stories = null;

    if(isset($_GET["storyId"])){

        $story = $storyTable->readById(addslashes($_GET["storyId"]));

        $storyId = (int)$story['id'];
        $storyTitle = $story['title'];
        $storyGenre = $genreTable->readById($story['genre_id'])["name"];
        $storyParts = $storyPartTable->getStoryPartsByStory($storyId, 0);

        echo "<li class='storyList list-group-item'>";

        echo "<h1>$storyTitle [$storyGenre]</h1>";
        foreach ($storyParts as $storyPart) {

            echo "<li class='list-group-item'>";

            $storyPartText = $storyPart['storyPartText'];
            $storyPartUser = $storyPart['storyPartCreator'];
            $storyPartDatetime = $storyPart['storyPartDate'];
            echo "<p>$storyPartText</p>";
	        echo "<span class='badge badge-default pull-right'>Created by $storyPartUser at $storyPartDatetime</span>";
	        echo "</li>";
	
        }

        echo "</li>";

        if(isset($_SESSION['user'])){

            echo "<li class='list-group-item'>";
            require_once 'addNewStoryPart.php';
            echo "</li>";

        }
    }
    else {
        if (isset($_GET["genre"])) {

            $stories = $storyTable->getStoriesByGenre(addslashes($_GET["genre"]));

        } else {

            $stories = $storyTable->getStories();

        }

        foreach ($stories as $story) {

            $storyId = (int)$story['storyId'];
            $storyTitle = $story['storyTitle'];
            $storyGenre = $story['genre'];
            $storyParts = $storyPartTable->getStoryPartsByStory($storyId, 3);

            echo "<a href='/?storyId=$storyId'><li class='storyList list-group-item'>";

            echo "<h1>$storyTitle [$storyGenre]</h1>";
            foreach ($storyParts as $storyPart) {

                echo "<li class='list-group-item'>";
                $storyPartText = $storyPart['storyPartText'];

                echo "<p>$storyPartText</p>";
                echo "</li>";

            }

            echo "</li></a>";
        }
    }

    ?>

</div>