
<div id="createStoryDiv" class="mx-auto text-center container">

    <h1>Add new story part</h1>

    <form class="text-left" action="/story/addNewStoryPart" method="post">

        <div class="form-group">
            <label for="storyPart">Your story part:</label>
            <textarea maxlength="200" rows="8" type="text" id="storyPart" name="storyPart" class="form-control"
                      placeholder="Write your story part here" required></textarea>
        </div>

        <div class="form-group">
            <?php
                $storyId = (int)$_GET["storyId"];
                echo "<input type='hidden' id='storyId' name='storyId' value=$storyId>";
            ?>
        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Save story part">
        </div>

    </form>

</div>
