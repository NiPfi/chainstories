<?php
/**
 * Created by PhpStorm.
 * User: bpfisn
 * Date: 01.06.17
 * Time: 13:39
 */
if(isset($_SESSION['success'])) {
	echo '<div class="alert alert-success alert-dismissable">';
	echo '<strong>';
    echo $_SESSION['success'];
    echo '</strong>';
    echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
    echo '</div>';
	unset($_SESSION['success']);
}
if(isset($_SESSION['error'])) {
	echo '<div class="alert alert-danger alert-dismissable">';
	echo '<strong>';
	echo $_SESSION['error'];
	echo '</strong>';
	echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
	echo '</div>';
	unset($_SESSION['error']);
}