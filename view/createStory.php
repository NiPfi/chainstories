
<div id="createStoryDiv" class="mx-auto text-center container">

    <h1>Create Story</h1>

    <form class="text-left" action="/story/create" method="post">

        <div class="form-group">
            <label for="storyTitle">Story Title</label>
            <input type="text" id="storyTitle" name="storyTitle" class="form-control"
                   placeholder="Write a cool title here" required>
        </div>
        
        <div class="form-group">
            <label for="story_genre_id">Genre</label>
            <select class="form-control" id="story_genre_id" name="story_genre_id">
                <?php
                    require_once "./../repositories/GenreTable.php";
                    
                    $table = new GenreTable();
                    
                    foreach ($table->readAll() as $genre) {
                        echo "<option value='$genre[id]'>$genre[name]</option>";
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label for="storyPart">Beginning of the story</label>
            <textarea maxlength="200" type="text" id="storyPart" name="storyPart" class="form-control"
                      placeholder="Start to write your story here" required></textarea>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Create Story">
        </div>
    </form>

</div>
