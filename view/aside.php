<?php

require_once './../repositories/GenreTable.php';

?>

<aside class="col-2">

    <ul class="list-group">

    <a href="/"> <li class="list-group-item">All Genres</li> </a>

    <?php

        $genreTable = new GenreTable();
        $genres = $genreTable->readAll();

        foreach($genres as $genre){

            echo "<a href='/?genre=$genre[name]'><li class='list-group-item'>$genre[name]</li></a>";

        }

    ?>

    </ul>

    <?php

    if (isset($_SESSION['user'])) {
        echo '<a href="/story/createStory" class="btn btn-default" role="button">Create Story</a>';
    }

    ?>


</aside>