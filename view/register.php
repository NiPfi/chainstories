
<div id="registerDiv" class="mx-auto text-center container">

    <h1>Register</h1>

    <form class="text-left" action="/user/create" method="post" onsubmit="validate()">

        <div class="form-group">
            <label for="registerUsername">Username</label>
            <input type="text" id="registerUsername" name="registerUsername" class="form-control"
                   placeholder="what should others call you?" required
	            <?php
                    if(isset($_SESSION['form'])) {
                        echo 'value='.$_SESSION["form"]["uname"];
                    }
	            ?>
            >
        </div>

        <div class="form-group">
            <label for="registerEmail">E-Mail</label>
            <input type="email" id="registerEmail" name="registerEmail" class="form-control"
                   placeholder="example@domain.tl" required
	            <?php
	            if(isset($_SESSION['form'])) {
                        echo 'value='.$_SESSION["form"]["email"];
                    }
	            ?>
            >
        </div>

        <div class="form-group">
            <label for="registerPassword">Password</label>
            <input type="password" id="registerPassword" name="registerPassword" class="form-control"
                   placeholder="some secure password" required>
        </div>

        <div class="form-group">
            <label for="repeatPassword">Re-enter password</label>
            <input type="password" id="repeatPassword" name="repeatPassword" class="form-control"
                   placeholder="the same as above" required>
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" id="registerAgree" name="registerAgree" class="form-check-input" required>
                I agree to follow the community rules
            </label>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Register">
        </div>
    </form>

</div>
