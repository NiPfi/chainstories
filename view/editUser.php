
<div id="registerDiv" class="mx-auto text-center container">

    <h1>Edit User</h1>

    <form class="text-left" action="/user/changeUserPassword" method="post" onsubmit="validate()">

        <?php

        echo "<h5>Username: ".$_SESSION["user"]["username"]."</h5><br>";
        echo "<h5>Email: ".$_SESSION["user"]["email"]."</h5><br>";

        ?>

        <div class="form-group">
            <label for="newPassword">Change Password</label>
            <input type="password" id="newPassword" name="newPassword" class="form-control"
                   placeholder="some secure password" required>
        </div>

        <div class="form-group">
            <label for="newPasswordRepeat">Re-enter password</label>
            <input type="password" id="newPasswordRepeat" name="newPasswordRepeat" class="form-control"
                   placeholder="the same as above" required>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Save Changes">
        </div>

    </form>

    <form class="text-left" action="/user/deleteUser" method="post" onsubmit="validate()">

        <div class="form-group">
            <input type="submit" class="btn btn-warning" value="Delete User">
        </div>

    </form>

</div>
