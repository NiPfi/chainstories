<?php

require_once '../repositories/UserTable.php';
require_once './../lib/View.php';

/**
 * @see DefaultController
 */
class UserController
{
	
	private $userTable;
	
	function __construct() {
		
		$this->userTable = new UserTable();
		
	}

    /**
     * @see DefaultController::index()
     */
	public function index()
    {
	    header('Location: /');
	    die();
    }

    /**
     * creates a new user in the database with the data coming from the post request
     */
    public function create()
    {
	    session_start();
	    switch($_SERVER['REQUEST_METHOD']) {
		    case 'POST':
			
			    unset($_SESSION['form']);
		    	
			    $form = [
			    	'uname'  =>  $_POST["registerUsername"],
				    'email'  =>  $_POST["registerEmail"],
				    'pass'   =>  $_POST["registerPassword"],
				    'repeat' =>  $_POST["repeatPassword"],
				    'agree'  =>  $_POST["registerAgree"]
			    ];
			    
			    $errors = $this->valid($form);
			    if(!isset($errors)) {
				    $user = array(
					    "username" => $form['uname'],
					    "email" => $form['email'],
					    "password" => password_hash($form['pass'], PASSWORD_BCRYPT)
				    );
				    try {
					    $this->userTable->addUser($user);
					    $_SESSION['success'] = "You have been registered. Please log in";
					    header('Location: /');
				    } catch (Exception $e) {
						$_SESSION['error'] = $e;
						header('Location: /user/register');
				    }
			    } else {
			    	
			    	$message = 'The server rejected the form because: ';
			    	foreach ($errors as $error) {
			    		$message = $message . $error .', ';
				    }
			    	$_SESSION['error'] = $message;
			    	unset($form["pass"]);
			    	unset($form["repeat"]);
			    	$_SESSION['form'] = $form;
			    	header('Location: /user/register');
			    }
			
			    break;
		    default:
			    header('Location: /user/register');
	    }
	    
	    die();
	
    }

    /**
     * creates the register View
     */
    public function register()
    {
        session_start();
        if(isset($_SESSION["user"])){
            header('Location: /');
            die();
        }
        $view = new View('registerView');
        $view->display();
    }

    /**
     * creates the edit User view
     */
    public function editUser(){
        session_start();
        if(!isset($_SESSION["user"])){
            header('Location: /');
            die();
        }
        $view = new View('userEditView');
        $view->display();
    }

    public function changeUserPassword(){
        session_start();
        if(!isset($_SESSION["user"])){
            header('Location: /');
            die();
        }

        switch($_SERVER['REQUEST_METHOD']) {
            case 'POST':

                $userId = $_SESSION["user"]["id"];
	            $form = [
	            	'pass' => $_POST["newPassword"],
		            'repeat' => $_POST["newPasswordRepeat"]
	            ];
                $errors = $this->valid($form);
	            if (!isset($errors)) {
		            $newPassword = password_hash($form['pass'], PASSWORD_BCRYPT);
		            try {
			            $this->userTable->updatePassword($userId,$newPassword);
			            $_SESSION['success'] = "Successfully changed password!";
			            header('Location: /');
		            } catch (Exception $e) {
						$_SESSION['error'] = $e;
						header('Location: /user/editUser');
		            }
	            } else {
		            $message = 'The server rejected the form because: ';
		            foreach ($errors as $error) {
			            $message = $message . $error .', ';
		            }
		            $_SESSION['error'] = $message;
		            header('Location: /user/editUser');
	            }
	            break;
	        default:
		        header('Location: /user/editUser');
        }

    }

    public function deleteUser(){
	    session_start();
	    if(!isset($_SESSION["user"])){
	        header('Location: /');
	        die();
        }
	
	    switch($_SERVER['REQUEST_METHOD']) {
            case 'POST':
	
	            $userId = $_SESSION["user"]["id"];
	            //TODO try catch in case stmt failed to execute
	            $this->userTable->deleteUser($userId);
	            $this->logout();
	            break;
	        default:
	            header('Location: /user/editUser');
        }
	
	
    }

    /**
     * validates the login data and if it is correct starts a session for that user
     */
    public function login() {
	    
    	$email = $_POST["loginEmail"];
    	$password = $_POST["loginPassword"];
    	$user = $this->userTable->readByEmail($email);
    	if (password_verify($password, $user["password"])) {
    		$user["password"] = null;
    		session_start();
    		$_SESSION['user'] = $user;
		    header('Location: /');
		    die();
	    } else {
    		session_start();
    		$_SESSION['error'] = "Invalid credentials";
		    header('Location: /');
		    die();
	    }
	
    }

    /**
     * destroys the session of a logged in user
     */
    public function  logout() {
		session_start();
		unset($_SESSION['user']);
		session_unset();
	    $_SESSION['success'] = "You've been logged out";
	    header('Location: /');
    }
	
	/**
	 * Validates all given form fields
	 * @param $form
	 */
	private function valid($form) {
		$error = null;
		foreach ($form as $key=>$value) {
			switch ($key) {
				case 'uname':
					if (empty($value)) {
						$error[] = 'Invalid username';
					} else {
						$table = new UserTable();
						if(!empty($table->readByUsername($value))) {
							$error[] = "Username already taken";
						}
					}
				break;
				case 'email':
					$email = '';
					require_once ('./../lib/regex.inc');
					if (!preg_match($email, $value)) {
						$error[] = 'Invalid E-Mail address';
					} else {
						$table = new UserTable();
						if (!empty($table->readByEmail($value))) {
							$error[] = "Email already registered";
						}
					}
				break;
				case 'pass':
					if (strlen($value)< 8) {
						$error[] = 'Password shorter than 8 characters';
					}
				break;
				case 'repeat':
					if (strcmp($value, $form['pass']) !== 0) {
						$error[] = 'Passwords do not match';
					}
				break;
				case 'agree':
					if (!isset($value)) {
						$error[] = 'Checkbox not checked';
					}
				break;
			}
		}
		return $error;
	}
}
