<?php

require_once '../repositories/StoryTable.php';
require_once '../repositories/StoryPartTable.php';
require_once './../lib/View.php';
session_start();

/**
 * Created by PhpStorm.
 * User: Jonas Meise
 * Date: 31.05.17
 * Time: 13:08
 *
 * The story controller handles the different actions concerning story creation and story view
 */
class StoryController{

    private $storyTable;
    private $storyPartTable;

    /**
     * StoryController constructor.
     */
    function __construct() {

        $this->storyTable = new StoryTable();
        $this->storyPartTable = new StoryPartTable();

    }

    /**
     * @see DefaultController::index()
     */
    public function index()
    {
        header('Location: /');
        die();
    }

    /**
     * gets story form data from the post request and creates a new story in the database
     */
    public function create()
    {
        switch($_SERVER['REQUEST_METHOD']) {
            case 'POST':

                $datetime = date('Y-m-d H:i:s');

                $story = array(

                    "story_title" => htmlspecialchars($_POST["storyTitle"]),
                    "story_datetime" => $datetime,
                    "story_genre_id" => $_POST["story_genre_id"]

                );

                //TODO transaction to prevent story creation without storypart
                $this->storyTable->addStory($story);

                $storyPart = array(

                    "storyPart_text" => htmlspecialchars($_POST["storyPart"]),
                    "storyPart_datetime" => $datetime,
                    "storyPart_user_id" => $_SESSION["user"]["id"],
                    "storyPart_story_id" => $this->storyTable->getStoryByDatetimeAndTitle($story["story_datetime"],$story["story_title"])["storyId"]

                );

                $this->storyPartTable->addStoryPart($storyPart);

                header('Location: /');
                break;
            default:
                header('Location: /story/createStory');
        }

        die();
    }

    /**
     * adds a new storyPart to the database, takes arguments and values from post request
     */
    public function addNewStoryPart(){

        switch($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                $storyPart = array(

                    "storyPart_text" => htmlspecialchars($_POST["storyPart"]),
                    "storyPart_datetime" => date('Y-m-d H:i:s'),
                    "storyPart_user_id" => $_SESSION["user"]["id"],
                    "storyPart_story_id" => htmlspecialchars($_POST["storyId"])

                );

                $this->storyPartTable->addStoryPart($storyPart);

                header("Location: /?storyId=".$storyPart["storyPart_story_id"]);
                break;
            default:
                header('Location: /');
        }

        die();
    }

    /**
     *creates the story View
     */
    public function createStory()
    {
	    
	    if(isset($_SESSION['user'])) {
		    $view = new View('createStoryView');
		    $view->display();
	    } else {
	        header("Location: /");
	        die();
	    }
    }

}