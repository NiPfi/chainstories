function validate() {
    removeErrors();

    var o = document.getElementById("registerUsername");
    checkNotEmpty(o);

    o = document.getElementById("registerEmail");
    validateEmail(o);

    var r = document.getElementById("registerPassword");
    validatePass(r);

    o = document.getElementById("repeatPassword");
    validateRepeat(o, r);

    o = document.getElementById("registerAgree");
    validateCheck(o);

}

function validateEmail(o) {
    if(checkNotEmpty(o)){
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // Quelle: emailregex.com
        if (o.value.match(pattern) === null) {
            event.preventDefault();
            setMessage(o,"Please enter a valid E-Mail address");
        }
    }
}

function validatePass(o) {
    if(o.value.length < 8) {
        event.preventDefault();
        setMessage(o, "Please enter at least 8 characters");
    }
}

function validateRepeat(o, r) {
    if(o.value !== r.value) {
        event.preventDefault();
        setMessage(o, "Please enter the same password as above");
    }
}

function validateCheck(o) {
    if (!o.checked) {
        event.preventDefault();
        setMessage(o,"Please check this box");
    }
}

function removeErrors() {
    var emsg = document.getElementsByClassName("form-control-feedback");
    for (;emsg.length > 0;) {
        var element = emsg[0];
        element.parentElement.removeChild(element);
    }

    var errors = document.getElementsByClassName("form-control-danger");
    for (;errors.length > 0;) {
        element = errors[0];
        element.classList.remove("form-control-danger");
    }

    errors = document.getElementsByClassName("has-danger");
    for (;errors.length > 0;) {
        element = errors[0];
        element.classList.remove("has-danger");
    }
}

function checkNotEmpty(o) {
    if (o.value.trim() === "") {
        event.preventDefault();
        setMessage(o,"Please fill out this field");
        return false;
    }
    return true;
}

function setMessage(o, message) {
    o.classList.add("form-control-danger");

    var par = o.parentElement;

    par.classList.add("has-danger");

    var errdiv = document.createElement("div");
    errdiv.setAttribute("class","form-control-feedback");
    errdiv.innerHTML=message;
    par.insertBefore(errdiv, o.nextSibling);
}
