<?php
/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 31.05.17
 * Time: 13:59
 *
 * The StoryPartTable is responsible for the database actions concerning the db Table "StoryPart"
 */
class StoryPartTable extends Database{

    /**
     * StoryPartTable constructor.
     */
    function __construct() {
        $this->tableName="StoryPart";
    }

    /**
     * insert a storyPart in the database with the given attributes
     * @param array $storyPart storyParts from the database
     */
    function addStoryPart(array $storyPart){

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare('INSERT INTO `'.$this->tableName.'` (`text`, `datetime`, `user_id`, `story_id`) VALUES (?,?,?,?);')) {
            $stmt->bind_param("ssii", $storyPart["storyPart_text"], $storyPart["storyPart_datetime"],$storyPart["storyPart_user_id"],$storyPart["storyPart_story_id"]);
            $stmt->execute();
	        $this->checkForError($stmt);
            $stmt->close();
        }
        $mysqli->close();

    }

    /**
     * gets all storyParts of a given Story from the database
     * @param int $storyId given StoryId
     * @param int $limitRows can be used to limit the number of returned storyParts. If limit = 0 no limit is set.
     * @return bool|mysqli_result|null  returns the fetched storyParts
     */
    function getStoryPartsByStory($storyId, $limitRows){

        $result = null;
        $stmt = null;

        $mysqli = $this->openConnection();

        if($limitRows == 0){
            $stmt = $mysqli->prepare("SELECT StoryPart.text as storyPartText,StoryPart.datetime as storyPartDate, User.username AS storyPartCreator
            FROM ($this->tableName)
            JOIN User ON StoryPart.user_id = User.id
            WHERE StoryPart.story_id=?");
            $stmt->bind_param("i",$storyId);
        }
        else{
            $stmt = $mysqli->prepare("SELECT StoryPart.text as storyPartText,StoryPart.datetime as storyPartDate
            FROM ($this->tableName)
            WHERE StoryPart.story_id=?
            LIMIT ?");
            $stmt->bind_param("ii",$storyId,$limitRows);
        }

        if ($stmt){
            $stmt->execute();
            $result = $stmt->get_result();
	        $this->checkForError($stmt);
            $stmt->close();
        }
        $mysqli->close();

        return $result;

    }

    /**
     * @see Database::getCols()
     */
	protected function getCols() {
		return ['id', 'text', 'date', 'user_id', 'story_id'];
	}
}