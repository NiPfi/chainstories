<?php

/**
 * Created by PhpStorm.
 * User: bpfisn
 * Date: 19.05.17
 * Time: 12:36
 */
class UserTable extends Database {

    /**
     * UserTable constructor.
     */
	function __construct() {
		$this->tableName="User";
	}

    /**
     * adds a to the database with usage of a prepared statement
     * @param array $user array with all needed user credentials to create a user
     */
	function addUser(array $user) {
		$username = $user["username"];
		$email = $user["email"];
		$password = $user["password"];
		
		$mysqli = $this->openConnection();

		if ($stmt = $mysqli->prepare('INSERT INTO `'.$this->tableName.'` (`username`, `email`, `password`) VALUES (?,?,?);')) {
			$stmt->bind_param("sss", $username, $email, $password);
			$stmt->execute();
			$this->checkForError($stmt);
			$stmt->close();
			
		}
		$mysqli->close();
		
	}

	function updatePassword($userId,$newPassword){

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare('UPDATE `'.$this->tableName.'` SET `password`=? WHERE `id`=?')) {
            $stmt->bind_param("si", $newPassword, $userId);
            $stmt->execute();
	        $this->checkForError($stmt);
            $stmt->close();
        }

        $mysqli->close();

    }

    function deleteUser($userId){

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare('DELETE FROM `'.$this->tableName.'` WHERE `id`=?')) {
            $stmt->bind_param("i", $userId);
            $stmt->execute();
            $stmt->close();
        }

        $mysqli->close();

    }

    /**
     * @see Database::getCols()
     */
	protected function getCols() {
		return ['id', 'username', 'email', 'password', 'enabled', 'role_id'];
	}
}