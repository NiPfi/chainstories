<?php
/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 31.05.17
 * Time: 11:25
 *
 * The StoryTable is responsible for the database actions concerning the db Table "Story"
 *
 */
class StoryTable extends Database{

    /**
     * StoryTable constructor.
     */
    function __construct() {
        $this->tableName="Story";
    }

    /**
     * insert a story in the database with the given attributes
     * @param array $story essential attributes for creating a story
     */
    function addStory(array $story){

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare('INSERT INTO `'.$this->tableName.'` (`title`, `datetime`, `genre_id`) VALUES (?,?,?);')) {
            $stmt->bind_param("ssi", $story["story_title"],$story["story_datetime"], $story["story_genre_id"]);
            $stmt->execute();
            $stmt->close();
        }
        $mysqli->close();

    }

    /**
     * //TODO eventually use a programmatic 'join' with a method from the genreTable to fetch the genres
     * get all stories from the database joined with the corresponding genre
     * @return bool|mysqli_result|null stories from the database
     */
    function getStories(){

        $result = null;

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare("SELECT Story.id as storyId,Story.title as storyTitle,Genre.name as genre FROM ($this->tableName)
        JOIN Genre on Story.genre_id=Genre.id;")) {
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
        }
        $mysqli->close();

        return $result;

    }

    /**
     * get only the stories from the database of the given genre
     * @param string $genre genre of which to get the stories
     * @return bool|mysqli_result|null stories of the given genre from the database
     */
    function getStoriesByGenre($genre){

        $result = null;

        $mysqli = $this->openConnection();

        if ($stmt = $mysqli->prepare(
        	"SELECT Story.id as storyId,Story.title as storyTitle,Genre.name as genre FROM ($this->tableName)
        			JOIN Genre on Story.genre_id=Genre.id
       				WHERE Genre.name=?;")) {
        	$stmt->bind_param("s", $genre);
            $stmt->execute();
            $result = $stmt->get_result();
	        $this->checkForError($stmt);
            $stmt->close();
        }
        $mysqli->close();

        return $result;

    }

    /**
     * get a specific story by it's datetime and title attribute. Those two attributes are unique together
     * @param string $datetime given datetime
     * @param string $title given title
     * @return mixed returns the matching story
     */
    function getStoryByDatetimeAndTitle($datetime, $title) {
    	$result = null;
    	
    	$mysqli = $this->openConnection();
    	
    	if($stmt = $mysqli->prepare(
    		"SELECT Story.id as storyId,Story.title as storyTitle,Genre.name as genre FROM ($this->tableName)
        			JOIN Genre on Story.genre_id=Genre.id
        			WHERE Story.datetime=? AND Story.title=?"
	    )) {
    		$stmt->bind_param("ss", $datetime, $title);
    		$stmt->execute();
    		$result = $stmt->get_result();
		    $this->checkForError($stmt);
    		$stmt->close();
	    }
	    $mysqli->close();
    	
    	return $result->fetch_array();
    }

    /**
     * @see Database::getCols()
     */
	protected function getCols() {
		return ['id', 'title', 'genre_id'];
	}
}