<?php
/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 31.05.17
 * Time: 14:27
 *
 * The GenreTable is responsible for the database actions concerning the db Table "Genre"
 */
class GenreTable extends Database{

    /**
     * GenreTable constructor.
     */
    function __construct() {
        $this->tableName="Genre";
    }

    /**
     * @see Database::getCols()
     */
	protected function getCols() {
		return ['id', 'name'];
	}
}